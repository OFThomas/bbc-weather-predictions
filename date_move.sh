cd /home/pi/Documents/bbc-weather-predictions/data/
# at the end of each day (midnight) cron moves all of the 
# data files into a new directory for that date.

#date get day, number of month & month
Day=$(date -d "yesterday" +%a)
Daynum=$(date -d "yesterday" +%d)
Month=$(date -d "yesterday" +%b)
filetocopy=$(date -d "yesterday" +%Y-%m-%d)

newdir=$Month$Daynum$Day
mkdir $newdir 

for file in *$filetocopy*.txt
do
    echo $file
    mv $file $newdir/$file
done

# call the file scrapping to extract relevant data 
echo $PWD/$newdir/

# extract compressed data set 
../scrape_files.sh $PWD/$newdir/

# extract each days prediction for each time 
../get_days_prediction.sh $newdir
