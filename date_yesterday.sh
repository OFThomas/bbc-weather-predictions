cd /home/pi/Documents/bbc-weather-predictions/data/
# at the end of each day (midnight) cron moves all of the 
# data files into a new directory for that date.

#date get day, number of month & month
Day=$(date -d "2 days ago" +%a)
Daynum=$(date -d "2 days ago" +%d)
Month=$(date -d "2 days ago" +%b)
filetocopy=$(date -d "2 days ago" +%Y-%m-%d)

newdir=$Month$Daynum$Day
mkdir $newdir 

for file in *$filetocopy*.txt
do
    echo $file
    mv $file $newdir/$file
done

# call the file scrapping to extract relevant data 
echo $PWD/$newdir/
../scrape_files.sh $PWD/$newdir/
