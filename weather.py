import datetime
import re
import threading
from urllib.request import urlopen

from bs4 import BeautifulSoup


def bbcweather(day=0):

    url = 'https://www.bbc.co.uk/weather/2654675/day' + day  # bristol
    rawBBCWeather = urlopen(url).read()
    soup = BeautifulSoup(rawBBCWeather, "html.parser")
    # don't question it
    htmlout = soup.findAll("li", class_="wr-time-slot wr-js-time-slot ")
    date = soup.find(
        class_="wr-day__title wr-js-day-content-title")["aria-label"]
    print("\n\n\n", date)

    #actual date & time from os
    now = datetime.datetime.now()
    current_time = now.strftime("%Y-%m-%d_%H%M")

    ## remove spaces from the date
    abs_file_path = "/home/pi/Documents/bbc-weather-predictions/data/"
    filename = abs_file_path + "bbcweather_" + date.replace(
        " ", "") + "_" + current_time.replace(" ", "") + ".txt"
    file = open(filename, "a+")  # append and create if doesn't exist

    # BBC = soup.findAll(string=re.compile("temperatureC"))
    # print("type soup", type(soup), " type bbc", type(BBC))

    # BBC = soup.head
    # print(BBC.get_text())

    BBC = []
    for lists in htmlout:
        text = lists.get_text()
        BBC.append(text)

    for i in BBC:
        print("BBC \n", i)
        file.write(i + "\n")

    strBBC = str(BBC)
    """ replaceStrings = [
        '&lt;p class="today"&gt;Today: ', '&lt;/p&gt;',
        '&lt;p lang="en-GB"&gt;'
    ]

    for items in replaceStrings:
        strBBC = re.sub(re.compile(items), "", str(strBBC))
    """
    #strBBC = strBBC.strip()

    #°
    #"\u00A2"

    #print(strBBC)
    return BBC


if __name__ == "__main__":
    format = "%Y-%m-%d"
    #    threading.Thread(target=metgroup).start()
    # threading.Thread(target=bbcweather).start()
    bbcweather("0")
    bbcweather("1")
