datadir=/home/pi/Documents/bbc-weather-predictions/data/
outdir=/home/pi/Documents/bbc-weather-predictions/
datadir=$1
outdir=$datadir

rm "$outdir"outputdata.dat
for file in $datadir*.txt
do
    echo $file 
    echo "" >> "$outdir"outputdata.dat
    echo "#" $file >> "$outdir"outputdata.dat
    python3 /home/pi/Documents/bbc-weather-predictions/regexing.py - <<<"$file" >> "$outdir"outputdata.dat
done 
