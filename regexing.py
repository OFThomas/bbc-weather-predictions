import csv
import re

file_path = str(input())
# file_path = "/home/oliver/Documents/bbc-weather-predictions/data/Jul27Sat/outputdata.dat"
# file_path = "/home/oliver/Documents/bbc-weather-predictions/data/Jul27Sat/bbcweather_Saturday27thJuly_2019-07-27_1300.txt"
# file_path = "/home/pi/Documents/bbc-weather-predictions/data/Jul27Sat/bbcweather_Saturday27thJuly_2019-07-27_2315.txt"

# this is need for the dates
Dict = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]

with open(file_path) as csvfile:
    readcsv = csv.reader(csvfile, delimiter=',')
    dates = []
    temps = []
    rain = []
    feels_like = []
    for row in readcsv:
        # print(row)
        dates.append(row[0])
        temps.append(row[1])
        rain.append(row[2])
        # feels_like.append(row[4])

    # now remove words from temp
    for i in range(0, len(temps)):
        # keep only temperature values
        temps[i] = ''.join(j for j in temps[i] if j.isdigit())
        # remove the last 2 as i'm pretty sure Fahrenheit is always 2 digits in the uk?
        temps[i] = temps[i][:-2] + "c"
        # remove words from rain
        rain[i] = ''.join(j for j in rain[i] if j.isdigit()) + "%"

    print("# time \t temp \t rain")

    # find which days weather we have.
    # there are
    day = []
    daypos = []
    days = 0
    daycounter = 0
    newdaypos = 0
    for i in range(0, len(dates)):
        words = " ".join(re.findall("[a-zA-Z]+", dates[i]))
        if words != "":  # ie there is a day
            day.append(words)
            daypos.append(i)
            days += 1

    daypos.append(len(dates))

    for k in range(0, daypos[0]):
        # oh dear i'm sorry this works...
        dates[k] += Dict[Dict.index(day[0]) - 1]

    # then for each day add the text to it
    for i in range(0, days):
        # skip 00:00 as it already has the day in text
        for j in range(daypos[i] + 1, daypos[i + 1]):
            dates[j] += day[i]

    for i in range(0, len(dates)):
        print(dates[i], "\t", temps[i], "\t", rain[i])
