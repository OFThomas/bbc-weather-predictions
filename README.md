# BBC weather predictions

## All of the results are in outputdata.dat 
Every 15 minutes the Pi downloads the weather information. Scrape_files.sh is run to get
only the time, temperature and chance of rain which is then appended to the
outputdata.dat file.

Still need to sort by days 

Using cron jobs on the pi for data retrieval

Crontab looks like,
HOME=/home/pi
SHELL=/bin/bash
PATH=/bin:/usr/bin:/usr/local/bin
# every 15 minutes 
_/15 \* \* \* \*  /usr/bin/python3 /home/Documents/bbc-weather-predictions/weather.py
_/15 \* \* \* \* /home/Documents/bbc-weather-predictions/scrape_files.sh
_/15 \* \* \* \* /home/pi/Documents/bbc-weather-predictions/git.sh >> /home/pi/Documents/bbc-weather-predictions/logs 2>&1

time
temperature
chance of rain
feels like temp
