import numpy as np
import pandas

try:
    input_chars = input('enter file')
    fname = str(input_chars)
    print(input_chars)
    print(fname)
except:
    print('A file reading error occured')
    fname = 'test_data.txt'

# fname = './data/bbcweather_Friday26thJuly_2019-07-26_1230.txt'

#read the entire file into a panda dataframe
#do it like this because numpy genfromtxt doesn't like the degrees symbol
#yes it's a hack, I'm not a programmer
all_data = pandas.read_csv(fname, delimiter = ',', dtype = str, names = [0,1,2,3,4])
# all_data = pandas.read_csv(fname

#take the second column of the data and convert to a numpy array
column_2 = all_data[[1]].to_numpy()

#create an array to store the final temperatures in
temperatures = np.zeros(column_2.shape[0])

#iterate down the column, extracting the number at the end of the 4th word
#this is all to get around the crazy format the BBC give us this data in

for i in range(column_2.shape[0]):
	#take the word that ends with the temperature in degrees C
	word = column_2[i,0].split()[3]
	#find the digits in it and write them to a list
	digit_list = [s for s in word if s.isdigit()]

	#iterate over that list to make an integer out of any digits found
	#and then place them into our temperatures array as an integer
	placeholder = ''
	for digit in digit_list:
		placeholder = placeholder + digit
	temperatures[i] = int(placeholder)

print(temperatures)


#This might be the hackiest code I've ever written, but it should work....
