dir=/home/pi/Documents/bbc-weather-predictions/data
filefolder=$1
path=$dir/$filefolder
cd $path

#day="Sun"
for day in Mon Tue Wed Thu Fri Sat Sun
do 
    rm plottimesorted$day.dat
    if [ "$(grep -cim1 $day outputdata.dat)" -eq 1 ]
    then 
        echo there is a $day
        for num in $(seq -w 0 23)
        do
            # echo $num":00"$day
            grep $num":00"$day outputdata.dat >> plottimesorted$day.dat
            echo >> plottimesorted$day.dat
        done
        # make files with each time stamp recording and double space after 23:00 for
        # gnuplot
        rm splot$day.dat
        grep $day outputdata.dat >> splot$day.dat
        # inset two new blank lines after every line with 23:00 in it (should be the end
        # of each day)
        sed -i '/23:00/{G;G}' splot$day.dat
    else 
        echo no $day
    fi
done 
